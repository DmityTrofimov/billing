from django.db import models


class Wallet(models.Model):

    amount = models.DecimalField(decimal_places=2, max_digits=12, default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Wallet'
        verbose_name_plural = 'Wallets'


class BaseTransaction(models.Model):

    amount = models.DecimalField(decimal_places=2, max_digits=12)

    foreign_transaction_id = models.CharField(max_length=64)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class DepositTransaction(BaseTransaction):

    wallet = models.ForeignKey(
        Wallet, models.CASCADE, related_name='deposit_transactions')

    class Meta:
        verbose_name = 'Deposit transaction'
        verbose_name_plural = 'Deposit transactions'


class WithdrawTransaction(BaseTransaction):

    from_wallet = models.ForeignKey(
        Wallet, models.CASCADE, related_name='outcome_withdraw_transactions')
    to_wallet = models.ForeignKey(
        Wallet, models.CASCADE, related_name='income_withdraw_transactions')

    class Meta:
        verbose_name = 'Withdraw transaction'
        verbose_name_plural = 'Withdraw transactions'
