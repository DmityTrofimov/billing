from decimal import Decimal

import trafaret as t


TransactionIdContract = t.String(max_length=64)


class AmountContract(t.Trafaret):
    def check_and_return(self, value):
        t.String().check(value)
        if t.ToFloat().check(value) <= 0:
            self._failure('should be above zero')
        return Decimal(value)


DepositContract = t.Dict({
    'transaction_id': TransactionIdContract,
    'wallet_id': int,
    'amount': AmountContract,
})

WithdrawContract = t.Dict({
    'transaction_id': TransactionIdContract,
    'from_wallet_id': int,
    'to_wallet_id': int,
    'amount': AmountContract,
})
