import json
import trafaret as t

from django.http import (
    JsonResponse,
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseNotFound,
)
from django.db import transaction
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from .contracts import DepositContract, WithdrawContract
from .models import Wallet, DepositTransaction, WithdrawTransaction


@method_decorator(csrf_exempt, name='dispatch')
class WalletNewView(View):

    def post(self, request):
        wallet = Wallet.objects.create()
        return JsonResponse({
            'wallet_id': wallet.pk,
        })

    def get(self, request, wallet_id):
        wallet = Wallet.objects.filter(pk=wallet_id).first()
        if wallet is None:
            return HttpResponseNotFound("Wallet not found")
        return JsonResponse({
            'wallet_id': wallet.pk,
            'amount': str(wallet.amount),
        })


@method_decorator(csrf_exempt, name='dispatch')
class DepositView(View):

    @transaction.atomic
    def post(self, request):
        try:
            payload = DepositContract.check(json.loads(request.body))
        except (t.DataError, json.JSONDecodeError) as e:
            return HttpResponseBadRequest(e)

        wallet = Wallet.objects.select_for_update().filter(
            pk=payload['wallet_id'],
        ).first()

        if wallet is None:
            return HttpResponseNotFound("Wallet not found")

        trx, created = DepositTransaction.objects.get_or_create(
            foreign_transaction_id=payload['transaction_id'],
            defaults={
                'wallet': wallet,
                'amount': payload['amount'],
            }
        )

        if not created:
            return HttpResponseBadRequest("Duplicated transactions")

        wallet.amount += trx.amount
        wallet.save(update_fields=('amount', 'updated_at'))

        return HttpResponse("Ok")


@method_decorator(csrf_exempt, name='dispatch')
class WithdrawView(View):

    @transaction.atomic
    def post(self, request):
        try:
            payload = WithdrawContract.check(json.loads(request.body))
        except (t.DataError, json.JSONDecodeError) as e:
            return HttpResponseBadRequest(e)

        from_wallet = Wallet.objects.select_for_update().filter(
            pk=payload['from_wallet_id'],
        ).first()

        if from_wallet is None:
            return HttpResponseNotFound("Outcome wallet not found")

        to_wallet = Wallet.objects.select_for_update().filter(
            pk=payload['to_wallet_id'],
        ).first()

        if to_wallet is None:
            return HttpResponseNotFound("Income wallet not found")

        amount = payload['amount']
        if amount > from_wallet.amount:
            return HttpResponseBadRequest("Insufficient balance")

        trx, created = WithdrawTransaction.objects.get_or_create(
            foreign_transaction_id=payload['transaction_id'],
            defaults={
                'from_wallet': from_wallet,
                'to_wallet': to_wallet,
                'amount': payload['amount'],
            }
        )

        if not created:
            return HttpResponseBadRequest("Duplicated transactions")

        from_wallet.amount -= amount
        from_wallet.save(update_fields=('amount', 'updated_at'))

        to_wallet.amount += amount
        to_wallet.save(update_fields=('amount', 'updated_at'))

        return HttpResponse("Ok")
