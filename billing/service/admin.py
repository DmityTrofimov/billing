from django.contrib import admin

from .models import Wallet, DepositTransaction, WithdrawTransaction


@admin.register(Wallet)
class WalletAdmin(admin.ModelAdmin):
    pass


@admin.register(DepositTransaction)
class DepositTransactionAdmin(admin.ModelAdmin):
    pass


@admin.register(WithdrawTransaction)
class WithdrawTransactionAdmin(admin.ModelAdmin):
    pass
