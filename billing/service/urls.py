from django.urls import path

from .views import WalletNewView, DepositView, WithdrawView

urlpatterns = [
    path('wallet/new', WalletNewView.as_view()),
    path('wallet/<int:wallet_id>', WalletNewView.as_view()),
    path('deposit', DepositView.as_view()),
    path('withdraw', WithdrawView.as_view()),
]
